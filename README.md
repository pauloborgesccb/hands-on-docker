# Hands On docker

## O que é docker?
De forma resumida, o Docker é uma plataforma de código aberto, desenvolvido na linguagem Go e criada pelo próprio Google. Por ser de alto desempenho, o software garante maior facilidade na criação e administração de ambientes isolados, garantindo a rápida disponibilização de programas para o usuário final.

## Qual utilidade?

- Não ter divergencias entre ambientes de desenvolvimento e publicados
- Abstrai varias implementações de infra

## Identificando Imagens oficiais
- Elas não tem prefixo, simples neh?! eu tbm acho :)
ex: pauloborges/node #Imagem não oficial
    node # Imagem oficial(geralmente mantido pelo idealizador do software, nesse cado o nodejs)

https://hub.docker.com/_/node/

## Documentação Oficial
https://www.docker.com/

https://docs.docker.com/

# Comandos Importantes

# Executar uma imagem
```$docker run <image-id>```

## Docker login
```$ docker login ```

## Baixando uma imagem
```$ docker pull node```

## Baixando subindo
```$ docker push node```

## Baixando uma imagem
```$ docker pull node```

## Lista todas as imagens existentes
```$ docker image ls```

## Inspecionar container
```$ docker inspect <container-id> ```
## Lista todos containers em execução
``` $ docker ps```

## Lista todos containers existentes
```$ docker ps -a ```

## Lista todos containers em execução
```$ docker ps -a ```

## Executar imagem docker
```$ docker exec -it  <container-id> bash ``` 

## Remove imagem
```$ docker rmi <image-id> ```

## Remove container
```$ docker rmi <container-id> ```

## Remove todos containers
```$ docker rm -vf $(docker ps -a -q) ```

## Remove todas imagens
```$ docker rmi -f $(docker images -a -q) ```

## Buildar imagem
```$ docker build . -t <image-id:image-tag> ```
